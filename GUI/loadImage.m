function loadImage( handles )

selectedDirNumber = get(handles.folderList,'Value');
uData = get(handles.MainGUI,'UserData');

dirName = uData.project.directories{selectedDirNumber}.dirName;

selectedImgNumber = get(handles.imgList,'Value');
imgStr = get(handles.imgList,'String');
imgName = imgStr{selectedImgNumber};

img = imread(fullfile(dirName,imgName));
uData.currentImage = img;
im = imagesc(img,'Parent',handles.axesCarImage);
set(im,'HitTest','off');
set(im,'ButtonDownFcn',{@clickOnImage, handles});
set(im,'HitTest','on');
set(handles.MainGUI,'UserData',uData);

end

