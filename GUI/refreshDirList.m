function refreshDirList( handles )

uData = get(handles.MainGUI,'UserData');

dirStr = cell(1,length(uData.project.directories));
for i=1:length(uData.project.directories)
    dirStr{i} = uData.project.directories{i}.dirName;
end

set(handles.folderList,'string',dirStr);
if ~isempty(uData.project.directories)
    set(handles.folderList,'value',1);
    refreshImgList(handles);
end

end

