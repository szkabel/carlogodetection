function clickOnImage( hObject, ~ ,handles )

uData = get(handles.MainGUI,'UserData');
selectedDir = get(handles.folderList,'Value');
selectedImg = get(handles.imgList,'Value');

axesPos = get(get(hObject,'Parent'),'Position');
pos = get(gcf,'CurrentPoint');

if ~isempty(uData.project.directories{selectedDir}.images{selectedImg})
    posInImg = pos - axesPos(1:2);
    posInImg(2) = axesPos(4)-posInImg(2);
    for i=1:length(uData.project.directories{selectedDir}.images{selectedImg})
        scaledBB = uData.project.directories{selectedDir}.images{selectedImg}{i}.bbox;
        xScale = axesPos(3)/size(uData.currentImage,2);
        yScale = axesPos(4)/size(uData.currentImage,1);
        scaledBB([1 3]) = xScale.*(scaledBB([1 3]));
        scaledBB([2 4]) = yScale.*(scaledBB([2 4]));
        if posInImg(1)>=scaledBB(1) && posInImg(1)<=scaledBB(1)+scaledBB(3) &&...
                posInImg(2)>=scaledBB(2) && posInImg(2)<=scaledBB(2)+scaledBB(2)
            
              [selection,isOk] = listdlg('PromptString','Select a class for this:',...
                      'SelectionMode','single',...
                      'ListString',uData.project.classes);
                  
              if isOk
                  uData.project.directories{selectedDir}.images{selectedImg}{i}.label = selection;
              end
            
        end
    end
end

set(handles.MainGUI,'UserData',uData);


end

