function refreshImgList(handles)

selectedDirNumber = get(handles.folderList,'Value');
uData = get(handles.MainGUI,'UserData');

dirName = uData.project.directories{selectedDirNumber}.dirName;

if isfield(uData.project.directories{selectedDirNumber},'images')
    new = 0;
else
    new = 1;
end

newImages = {};

imgExtentions = {'png','jpg','tiff'};

d = dir(fullfile(dirName));

imgList = cell(1,length(d));

k = 1;
for i=1:length(d)
    isImg = 0;
    [~,~,ext] = fileparts(d(i).name);
    for j=1:length(imgExtentions)
        if strcmpi(ext(2:end),imgExtentions{j})
            isImg = 1;
            break;
        end
    end
    if isImg
        imgList{k} = d(i).name;
        if new
            newImages{k} = [];
        else
            for j=1:length(uData.project.directories{selectedDirNumber}.images)
                if strcmp(d(i).name,uData.project.directories{selectedDirNumber}.imgList{j})
                    newImages{k} = uData.project.directories{selectedDirNumber}.images{j};
                    break;
                end
            end
            if length(newImages)<k
                newImages{k} = [];
            end
        end
        k = k+1;
    end
end
imgList(k:end) = [];
uData.project.directories{selectedDirNumber}.imgList = imgList;
uData.project.directories{selectedDirNumber}.images = newImages;

currentSelected = get(handles.imgList,'Value');
if currentSelected>length(imgList)
    set(handles.imgList,'Value',1);
end
set(handles.imgList,'String',imgList);

set(handles.MainGUI,'UserData',uData);


end