function varargout = DetectLogoGUI(varargin)
% DETECTLOGOGUI MATLAB code for DetectLogoGUI.fig
%      DETECTLOGOGUI, by itself, creates a new DETECTLOGOGUI or raises the existing
%      singleton*.
%
%      H = DETECTLOGOGUI returns the handle to a new DETECTLOGOGUI or the handle to
%      the existing singleton*.
%
%      DETECTLOGOGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in DETECTLOGOGUI.M with the given input arguments.
%
%      DETECTLOGOGUI('Property','Value',...) creates a new DETECTLOGOGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before DetectLogoGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to DetectLogoGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help DetectLogoGUI

% Last Modified by GUIDE v2.5 26-Nov-2017 22:50:28

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @DetectLogoGUI_OpeningFcn, ...
                   'gui_OutputFcn',  @DetectLogoGUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before DetectLogoGUI is made visible.
function DetectLogoGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to DetectLogoGUI (see VARARGIN)

% Choose default command line output for DetectLogoGUI
handles.output = hObject;

uData = get(handles.MainGUI,'UserData');
uData.project = [];
set(handles.MainGUI,'UserData',uData);

img = imread('logos.jpg');
imshow(img,'Parent',handles.axesCarImage);

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes DetectLogoGUI wait for user response (see UIRESUME)
% uiwait(handles.MainGUI);


% --- Outputs from this function are returned to the command line.
function varargout = DetectLogoGUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in folderList.
function folderList_Callback(hObject, eventdata, handles)
% hObject    handle to folderList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns folderList contents as cell array
%        contents{get(hObject,'Value')} returns selected item from folderList

refreshImgList(handles);
loadImage(handles);



% --- Executes during object creation, after setting all properties.
function folderList_CreateFcn(hObject, eventdata, handles)
% hObject    handle to folderList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in imgList.
function imgList_Callback(hObject, eventdata, handles)
% hObject    handle to imgList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns imgList contents as cell array
%        contents{get(hObject,'Value')} returns selected item from imgList

loadImage(handles);


% --- Executes during object creation, after setting all properties.
function imgList_CreateFcn(hObject, eventdata, handles)
% hObject    handle to imgList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --------------------------------------------------------------------
function uipushtool2_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to uipushtool2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%NEW

uData = get(handles.MainGUI,'UserData');

[firstDir] = uigetdir(pwd,'Select a folder with images');
uData.project = [];

if firstDir
    uData.project.directories{1}.dirName = firstDir;
    classes = {};    
    
    s = inputdlg('Please specify the classes (the end sign is ''thresh''):');
    while ~strcmp(s,'thresh') && ~isempty(s)
        classes{end+1} = s;
        s = inputdlg('Please specify the classes (the end sign is ''thresh''):');
    end
    
    if ~isempty(s)
        uData.project.classes = classes;
        set(handles.MainGUI,'UserData',uData);
        refreshDirList(handles);
    end
end


% --------------------------------------------------------------------
function addFolderPushtool_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to addFolderPushtool (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

uData = get(handles.MainGUI,'UserData');

[firstDir] = uigetdir(pwd,'Select a folder with images');

if firstDir
    uData.project.directories{end+1}.dirName = firstDir;
    set(handles.MainGUI,'UserData',uData);
    refreshDirList(handles);
end


% --------------------------------------------------------------------
function uipushtool4_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to uipushtool4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%SAVE
[fileName,pathName] = uiputfile('.mat','Save the project as:');

if fileName
    uData = get(handles.MainGUI,'UserData');

    project = uData.project; %#ok<NASGU> used in save
    save(fullfile(pathName,fileName),'project');
end


% --------------------------------------------------------------------
function uipushtool3_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to uipushtool3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%OPEN

[fileName, pathName] = uigetfile('.mat','Select a project to open');

if fileName
    uData = get(handles.MainGUI,'UserData');
    vars = load(fullfile(pathName,fileName));
    if isfield(vars,'project')
        uData.project = vars.project; %coming from a proper load
        set(handles.MainGUI,'UserData',uData);
        refreshDirList(handles);
    end
end


% --------------------------------------------------------------------
function detectOnImagePushtool_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to detectOnImagePushtool (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

uData = get(handles.MainGUI,'UserData');
selectedDir = get(handles.folderList,'Value');
selectedImg = get(handles.imgList,'Value');
if isfield(uData,'currentImage')
    if isempty(uData.project.directories{selectedDir}.images{selectedImg})
        screensize = get(0,'Screensize');
        infoD = dialog('Name','Detecting car logo','Position',[screensize(3)/2-180, screensize(4)/2-30,360,60]);
        uicontrol('Parent',infoD,'Style','text','Units','Pixels','Position',[10 20 340 20],'String','Searching for car logo on the image...');        
        pause(0.1);
        logos = detectLogoOnImage(uData.currentImage);
        if ishandle(infoD), close(infoD); end        
        uData.project.directories{selectedDir}.images{selectedImg} = logos;
    else
        logos = uData.project.directories{selectedDir}.images{selectedImg};
    end
    if ~isempty(logos)
        for i=1:length(logos)
            rectangle('Position',logos{i}.bbox,'EdgeColor','c','LineWidth',2,'Parent',handles.axesCarImage);
        end
    end
end

set(handles.MainGUI,'UserData',uData);


% --------------------------------------------------------------------
function detectAllImagePushtool_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to detectAllImagePushtool (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

uData = get(handles.MainGUI,'UserData');

for i=1:length(uData.project.directories)
    h = waitbar(0,['Searching logos on images in folder: ' num2str(i) ' 0%']);
    for j=1:length(uData.project.directories{i}.images)
        if isempty(uData.project.directories{i}.images{j})
            img = imread(fullfile(uData.project.directories{i}.dirName,uData.project.directories{i}.imgList{j}));
            logos = detectLogoOnImage(img);
            uData.project.directories{i}.images{j} = logos;
        end
        waitbar(j/length(uData.project.directories{i}.images),h,['Searching logos on images in folder: ' num2str(i) ' ' num2str(j/length(uData.project.directories{i}.images)*100,'%2.f') '%'])
    end    
    if ishandle(h), close(h); end
end

set(handles.MainGUI,'UserData',uData);


% --------------------------------------------------------------------
function trainClassifier_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to trainClassifier (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

uData = get(handles.MainGUI,'UserData');

tmpDir = uigetdir(pwd,'Please specify a temporary directory for the training set');

if ~tmpDir    
    return;
end


for i=1:length(uData.project.classes)
    if ~isdir(fullfile(tmpDir,uData.project.classes{i}))
        rmdir(fullfile(tmpDir,uData.project.classes{i}),'s');
        mkdir(fullfile(tmpDir,uData.project.classes{i}));
    end
end

screensize = get(0,'Screensize');
infoD = dialog('Name','Machine learning','Position',[screensize(3)/2-180, screensize(4)/2-30,360,60]);
t = uicontrol('Parent',infoD,'Style','text','Units','Pixels','Position',[10 20 340 20],'String','Creating training set...');
pause(0.1);
ii = 1;
for i=1:length(uData.project.directories)
    for j=1:length(uData.project.directories{i}.images)
        if ~isempty(uData.project.directories{i}.images{j})
            for k=1:length(uData.project.directories{i}.images{j})
                logo = uData.project.directories{i}.images{j}{k};
                if isfield(logo,'label')                    
                    imwrite(logo.img,fullfile(tmpDir,uData.project.classes{logo.label},[num2str(ii,'%03d') '.png']));
                    ii = ii+1;
                end
            end
        end
    end
end

if ishandle(infoD), set(t,'String','Training model'); pause(0.1); end

allImages = imageDatastore(tmpDir,'IncludeSubFolders', true, 'LabelSource','foldernames');
cnn = trainCNN(allImages, length(uData.project.classes));

uData.project.model = cnn;
set(handles.MainGUI,'UserData',uData);

if ishandle(infoD), set(t,'String','Model is trained and stored'); end
pause(2);
if ishandle(infoD), close(infoD); end;


% --------------------------------------------------------------------
function predictPushtool_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to predictPushtool (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

uData = get(handles.MainGUI,'UserData');
selectedDir = get(handles.folderList,'Value');
selectedImg = get(handles.imgList,'Value');

logos = uData.project.directories{selectedDir}.images{selectedImg};

if ~isempty(logos) && isfield(uData.project,'model')
    for i=1:length(logos)
        imgResized = imresize(logos{i}.img,[227 227]);
        label = classify(uData.project.model, imgResized);
        label = cellstr(label);
        text(handles.axesCarImage,logos{i}.bbox(1),logos{i}.bbox(2),label{1},'FontWeight','Bold','Color','w');
    end
end
