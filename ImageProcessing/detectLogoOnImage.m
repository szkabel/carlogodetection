function [logos] = detectLogoOnImage(img)
% NAME: SauvolaThreshold
% AUTHOR: Abel Szkalisity
% DATE: 2017. November 26.
%
%   DetectLogo on image. Puts together the LPR and the logo finding
%   functionalities
%   
%   INPUTS:
%       img             An RGB image on which to detect logos
%   OUTPUT:
%       logos           A cellarray of possible logos on the image. Each
%                       entry is a structure with the logo image (.img) and
%                       the bounding box (.bbox) of it. The image is
%                       colored and in original resolution.
%       
%
% Copyright 2017. Abel Szkalisity
% Advanced Image Processing Course
% University of Szeged


imgGray = rgb2gray(img);
downScaleFactor = 0.5;
imgSmall = imresize(imgGray,downScaleFactor);

licensePlates = detectLicensePlate(imgSmall);

nofCandidates = length(licensePlates);

bboxes = cell(1,nofCandidates);
logos = cell(1,nofCandidates);
k = 1;
for i=1:nofCandidates
    bboxes{i} = detectLogo(imgSmall,licensePlates{i});
    if length(bboxes{i}) == 4
        logos{k}.bbox = bboxes{i}*(1/downScaleFactor);
        [rowCoordinates, colCoordinates ] = extractPositionsFromBB( logos{k}.bbox );
        logos{k}.img = img(rowCoordinates,colCoordinates,:);
        k = k + 1;
    end
end
logos(k:end) = [];

end