function [ logoBoundingBox ] = detectLogo( img, boundingBox )
% NAME: detectLicencePlate
% AUTHOR: Kristof Mateffy
% DATE: 2017. November 25.
%
% Detect the logo based on the bounding box of the licence plate. We use a
% convolution mask to detect horizontal irregularities in the image. The
% plate bounding box is rejected if it is too close to the top edge.
%   
%   INPUTS:
%       img                 A grayscale relatively small image on which the
%                           logo will be detected
%       boundingBox         The bounding box for a license plate on the
%                           image
%   OUTPUT:
%       logoBoundingBox     A bounding box for the logo detected. If no
%                           reasonable logo can be detected then -1.
%
% Copyright 2017. Mateffy Kristof
% Advanced Image Processing Course
% University of Szeged

%we are searching for the LP within this height
LPmultiplier = 5;
noiseRemovalFactor = 3;

[rowPositions,colPositions] = extractPositionsFromBB(boundingBox);

heightOfLP = length(rowPositions);

top  = max(1,rowPositions(1)-heightOfLP*LPmultiplier);
bottom = rowPositions(1);

if bottom - top < 50
    logoBoundingBox = -1;
    return;
end

RIO = img(top:bottom,colPositions); %RIO instead of ROI ;)

mask = [0 0 0; -.5 1 -.5; 0 0 0];

convolved = conv2(RIO,mask,'same');

%cut off vertical edges
convolved(:,1:10) = 0;
convolved(:,end-10:end) = 0;

data = convolved(:);
M = mean(data);
S = std(data);

segmented = zeros(size(RIO));
segmented(abs(convolved) > M + noiseRemovalFactor * S) = 1; % threshold

st = strel('square',10);

segmented = imclose(segmented,st);

R = regionprops(logical(segmented),'Area','BoundingBox','PixelIdxList');
maxpix = zeros(1,length(R));
for k = 1:length(R)
    maxpix(k) = max(convolved(R(k).PixelIdxList));
end
[~,maxIdx] = max(maxpix);

logoBoundingBox = R(maxIdx).BoundingBox;
%Add 5% extra to each side to have a better view
logoBoundingBox(1) = logoBoundingBox(1)-logoBoundingBox(3)*0.05;
logoBoundingBox(2) = logoBoundingBox(2)-logoBoundingBox(4)*0.05;
logoBoundingBox(3) = logoBoundingBox(3)*1.1;
logoBoundingBox(4) = logoBoundingBox(4)*1.1;
logoBoundingBox(1) = boundingBox(1)+logoBoundingBox(1);
logoBoundingBox(2) = top+logoBoundingBox(2);

end

