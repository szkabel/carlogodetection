function [ boundingBox ] = detectLicensePlate( imgSmall )
% NAME: detectLicencePlate
% AUTHOR: Abel Szkalisity
% DATE: 2017. November 25.
%
% The function for detecting the bounding boxes of the licence plates. It
% is based on Anagnostopulos scw method and extended with heuristics based
% on the fact that the license plate is white and that its Constrast is
% usually high. The function has a lot of empricially defined
% parameters some of which are listed as variables at the start of the code
%   
%   INPUTS:
%       imgSmall            A grayscale relatively small image on which the
%                           LP will be detected
%   OUTPUT:
%       boundingBox         A cellarray of bounding boxes for the possible
%                           licesnce plates.
%
% Copyright 2017. Abel Szkalisity
% Advanced Image Processing Course
% University of Szeged

scwThreshold = 1;
SauvolaThres = .3;
SizeOfJointRegions = 128;
binarizedRatioThreshold = .5;
HaralickContrastThresh = 10;

load('TrainedSVM.mat');

    boundingBox = {};
    mask = scw(imgSmall,1,4,40,160,scwThreshold,'mean');
    I2 = imgSmall;
    I2(~mask) = 0;
    binarized = SauvolaThreshold(I2,SauvolaThres,SizeOfJointRegions,11);

    conncomp = bwconncomp(binarized,4);
    T = struct2table(regionprops(conncomp,{'BoundingBox','EulerNumber','Orientation'}));    
    T = T(T.EulerNumber < -3,:);
    T = T(T.BoundingBox(:,3)./T.BoundingBox(:,4) < 5.5 & T.BoundingBox(:,3)./T.BoundingBox(:,4) > 2.5 ,:);
    T = T(abs(T.Orientation)<35,:);
        
    i = 1;
    while i<=size(T,1)
        [rP,cP] = extractPositionsFromBB(T.BoundingBox(i,:));
        mask = zeros(size(binarized));
        mask(rP,cP) = 1;
        currFeatures = CalculateHaralick(imgSmall,logical(mask),5);
        warning off all
        isPlate = svmclassify(trainedSVM,currFeatures); %#ok<SVMCLASSIFY> the project should run also on older versions
        warning on all
        %empirical definition of plates from Haralick Features
        if mean2(binarized(rP,cP)  ) < binarizedRatioThreshold || currFeatures(2) < HaralickContrastThresh || ~isPlate
            T(i,:) = [];        
        else         
            boundingBox{i} = T.BoundingBox(i,:); %#ok<AGROW> not that much and the data is small
            i = i + 1;
        end
    end   

end

