function threshImg = SauvolaThreshold(img,k,R,b)
% NAME: SauvolaThreshold
% AUTHOR: Abel Szkalisity
% DATE: 2017. November 05.
%
%   Implementation of Sauvola local adaptive thresholding described in the
%   Anagnostopulos paper. The threshold value for each pixel is defined by
%       T(x,y) = m(x,y) + 1 + k*(sigma(x,y)/R-1)
%   
%   INPUTS:
%       img             original grayscale image
%       k,R             parameters of the local adaptive threshold formula
%       b               The size of the sliding window for the local
%                       adaptive threshold
%   OUTPUT:
%       threshImg       The binarized image
%
% Copyright 2017. Abel Szkalisity
% Advanced Image Processing Course
% University of Szeged

kernel = ones(b)/(b^2);

m = conv2(double(img),double(kernel),'same');
sigma = stdfilt(img,ones(b));

T = ones(size(m)) + m + k.*(sigma./R-ones(size(m)));

threshImg = img>T;

