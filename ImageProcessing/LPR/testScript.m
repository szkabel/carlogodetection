%script
load('TrainedSVM.mat');
% {
destPath = '..\..\..\..\Images\Results';

paths = {'..\..\..\..\Images\Toyota\',...  
    '..\..\..\..\Images\Wolksvagen\',... 
    '..\..\..\..\Images\Skoda\',...
    '..\..\..\..\Images\Opel\',...
    '..\..\..\..\Images\Merci\'...
    };


for iv = 1:length(paths)
    d = dir([ paths{iv} '*.JPG']);
    for iii = 1:length(d)
        img = imread([paths{iv} d(iii).name]);
%}
        img = imread('..\..\..\..\Images\Wolksvagen\DSCF5229.jpg');
        imgGray = rgb2gray(img);
        imgSmall = imresize(imgGray,0.5);

        mask = scw(imgSmall,1,4,40,160,1,'mean');
        I2 = imgSmall;
        I2(~mask) = 0;
        binarized = SauvolaThreshold(I2,.3,128,11);

        conncomp = bwconncomp(binarized,4);
        T = struct2table(regionprops(conncomp,{'BoundingBox','EulerNumber','Orientation'}));
        T = T(T.EulerNumber < -3,:);
        T = T(T.BoundingBox(:,3)./T.BoundingBox(:,4) < 5.5 & T.BoundingBox(:,3)./T.BoundingBox(:,4) > 2.5 ,:);
        T = T(abs(T.Orientation)<35,:);

        h = figure;
        warning off all
        imshow(imgSmall);
        warning on all

        %global PlateHaralickFeatures;
        %global NonPlateHaralickFeatures;
        i = 1;
        while i<=size(T,1)
            [rP,cP] = extractPositionsFromBB(T.BoundingBox(i,:));
            mask = zeros(size(binarized));
            mask(rP,cP) = 1;
            currFeatures = CalculateHaralick(imgSmall,logical(mask),5);
            warning off all
            isPlate = svmclassify(trainedSVM,currFeatures); %#ok<SVMCLASSIFY> the project should run also on older versions
            warning on all
            %empirical definition of plates from Haralick Features
            if mean2(binarized(rP,cP)  ) < 0.5 || currFeatures(2) < 10 || ~isPlate
                T(i,:) = [];        
            else            
                rectangle('Position',T.BoundingBox(i,:),'EdgeColor','r','LineWidth',1);

                logoBoundingBox = detectLogo( imgSmall, T.BoundingBox(i,:) );
                if length(logoBoundingBox) == 4
                    rectangle('Position',logoBoundingBox,'EdgeColor','g','LineWidth',2);
                else
                    disp('Couldn''t detect logo');
                end
         %       a = input('Is it a plate?\n');
         %       if a
         %          PlateHaralickFeatures{end + 1} = currFeatures;
         %       else
         %           NonPlateHaralickFeatures{end + 1} = currFeatures;
         %       end
                i = i + 1;
            end
        end
% {        
        [~,name,~] = fileparts(d(iii).name);
        saveas(h,fullfile(destPath,[name '.png']));
        
        close(h);

    end
end
% }