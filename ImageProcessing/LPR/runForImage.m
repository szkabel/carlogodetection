function runForImage(imgName,destPath)
%script

img = imread(imgName);
imgGray = rgb2gray(img);
imgSmall = imresize(imgGray,0.5);

mask = scw(imgSmall,1,4,40,160,1,'mean');
I2 = imgSmall;
I2(~mask) = 0;
binarized = SauvolaThreshold(I2,.5,128,11);

T = struct2table(regionprops(binarized,'All'));
T = T(T.EulerNumber < -3,:);
T = T(T.BoundingBox(:,3)./T.BoundingBox(:,4) < 5 & T.BoundingBox(:,3)./T.BoundingBox(:,4) > 3 ,:);
T = T(abs(T.Orientation)<35,:);

i = 1;
while i<=size(T,1)
    if mean2(binarized( ceil(T.BoundingBox(i,2)):ceil(T.BoundingBox(i,2))+floor(T.BoundingBox(i,4))-1,...
            ceil(T.BoundingBox(i,1)):ceil(T.BoundingBox(i,1))+floor(T.BoundingBox(i,3))-1  )  ) < 0.5
        T(i,:) = [];        
    else
        i = i + 1;
    end
end

h = figure;
imshow(imgSmall);
%{
max = -Inf;
best = [];
for i=1:size(T,1)    
    v = std(double(imgSmall(T.PixelIdxList{i}))); 
    if v>max
        max = v;
        best = T(i,:);
    end
end

rectangle('Position',best.BoundingBox,'EdgeColor','r','LineWidth',2);
%}
for i=1:size(T,1)
    rectangle('Position',T.BoundingBox(i,:),'EdgeColor','r','LineWidth',2);
end
%}

[~,name,~] = fileparts(imgName);
saveas(h,fullfile(destPath,[name '.png']));

close(h);

end