function Iand = scw(origImage,X1,Y1,X2,Y2,threshold,method)
% NAME: scw (Sliding Concentric Windows)
% AUTHOR: Abel Szkalisity
% DATE: 2017. November 05.
%
%   Implementation of the sliding concentric windows segmentation method
%   based on Anagnostopoulos et al. paper from 2006. This function
%   identifies local irregularities on the image which is the potential
%   point for the license plate.
%   Ma and Mb were changed, probably there was a mistake in the paper.
%   
%   INPUTS:
%       origImage       The original image that needs to be gray-scale
%       X1,Y1,X2,Y2     The parameters for the concentric sliding windows.
%                       X1 < X2 and Y1 < Y2, and X1<size(origImage,1),
%                       Y1<size(origImage,2) --> X refers to vertical axis
%                       and Y refers to horizontal axis. The ratio of X1/Y1
%                       supposed to be similar to the ratio of the object
%                       to be identified (in this case the license plate)
%       threshold       The threshold value for the segmentation
%       method          String 'mean' or 'std' to indicate if you wish to
%                       take mean as a window or std as a window statistic
%   OUTPUT:
%       Iand            A binary image representing the segmented regions.
%
% Copyright 2017. Abel Szkalisity
% Advanced Image Processing Course
% University of Szeged

[d1,d2] = size(origImage);

%init steps
Ma = zeros(d1,d2);
Mb = Ma;
Iand = Ma; 

%
%fast methods
kernelA = ones(2*X1,2*Y1)/(2*X1*2*Y1);
kernelB = ones(2*X2,2*Y2)/(2*X2*2*Y2);
if strcmp(method,'mean')
    %calculate mean    

    Ma = conv2(double(origImage),double(kernelA),'same');
    Mb = conv2(double(origImage),double(kernelB),'same');
elseif strcmp(method,'std')
   %calculate standard deviation
   Ma = stdfilt(grayImage, ones(2*X1,2*Y1)); 
   Mb = stdfilt(grayImage, ones(2*X2,2*Y2));
end

%{

%slow methods

percent = 0;
h = waitbar(percent,sprintf('Running SCW %2.0f%%',percent*100));

for i=X2+1:d1-(X2+1)
    for j=Y2+1:d2-(Y2+1)
        if strcmp(method,'mean')
            Ma(i,j) = mean2(origImage(i-X1:i+X1,j-Y1:j+Y1));
            Mb(i,j) = mean2(origImage(i-X2:i+X2,j-Y2:j+Y2));
        elseif strcmp(method,'std')
            Ma(i,j) = std2(origImage(i-X1:i+X1,j-Y1:j+Y1));
            Mb(i,j) = std2(origImage(i-X2:i+X2,j-Y2:j+Y2));            
        end
    end
    percent = (i-X2)/(d1-X2-1);
    waitbar(percent,h,sprintf('Running SCW %2.0f%%',percent*100));
end

if ishandle(h), close(h); end;
%}

%output image
Iand(Mb>0) = (Ma(Mb>0)./Mb(Mb>0)) > threshold;

end