function runForFolder( folderPath, imgExt)
%runs the pipeline for all the images in the given folder

d = dir([folderPath '\*.' imgExt]);
resDir = fullfile(folderPath,'results');
if ~exist(resDir,'dir')
    mkdir(resDir);
end

for i=1:length(d)
    runForImage(fullfile(folderPath,d(i).name),resDir);
end

end

