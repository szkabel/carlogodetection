function [ rowCoordinates, colCoordinates ] = extractPositionsFromBB( boundingBox )
%gives back the row and column coordinates from a bounding box

rowCoordinates = ceil(boundingBox(2)):ceil(boundingBox(2))+floor(boundingBox(4))-1;
colCoordinates = ceil(boundingBox(1)):ceil(boundingBox(1))+floor(boundingBox(3))-1;

end

