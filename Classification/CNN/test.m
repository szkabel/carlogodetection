allImages = imageDatastore('../../trainingSet', 'IncludeSubfolders', true, 'LabelSource', 'foldernames');
allImages.ReadFcn = @readFunctionTrain;
[trainingImages, testImages] = splitEachLabel(allImages, 0.8, 'randomize');

alex = alexnet;
layers = alex.Layers
layers(23) = fullyConnectedLayer(5);
layers(25) = classificationLayer

opts = trainingOptions('sgdm', 'InitialLearnRate', 0.001, 'MaxEpochs', 20, 'MiniBatchSize', 64);
myNet = trainNetwork(trainingImages, layers, opts);

predictedLabels = classify(myNet, testImages);
accuracy = mean(predictedLabels == testImages.Labels)
