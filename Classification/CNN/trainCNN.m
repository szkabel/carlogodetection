function cnn = trainCNN( trainingImages, classCount )
    % copy alexnet
    alex = alexnet;
    layers = alex.Layers;
    % set the result size
    layers(23) = fullyConnectedLayer(classCount);
    layers(25) = classificationLayer;

    opts = trainingOptions('sgdm', 'InitialLearnRate', 0.001, 'MaxEpochs', 20, 'MiniBatchSize', 64);
    % store read function of dataset
    oldFcn = trainingImages.ReadFcn;
    % set the new one
    trainingImages.ReadFcn = @readFunctionTrain;
    % do the training
    cnn = trainNetwork(trainingImages, layers, opts);
    % restore old read function
    trainingImages.ReadFcn = oldFcn;
end
